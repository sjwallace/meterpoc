# Meter Demo

To create a new Meter() instance you can call new Meter({...args}) with any overrides that pertain to drawing an arc. These accepted values are of the following:

 - rx
 - ry
 - x-axis-rotation
 - large-arc-flag
 - sweep-flag
 - x
 - y

The above can be passed into the new instance via:

```javascript
{ rx = 150, ry = 150, xRotation = 0, lArc = 0, sweep = 1, x = 0, y = 0, range = {} }
```
The range attribute determines the min, max, and values that will be drawn accordingly on the arc itself.

Once you've instantiated the Meter the ``` draw() ``` method accepts additional params that affect the meter's styling such as:

- fill
- stroke
- strokeWidth
- strokeOpacity

In addition to the above if you provide ``` {animated : true} ``` you'll get an animated arc when it's drawn and this animation happens immediately.

Also, if there has been an error that would prevent the Meter from being drawn correctly you can also pass in ``` {error : true} ``` and this will put the meter in a disabled state with an error message provided

Example Setup

### Installation

To install the stable version:

```
npm install
```

To run the application

```
npm start
```

To run the unit tests

```
npm test
```

That’s it!


### License

MIT
