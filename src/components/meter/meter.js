import Vivus from 'vivus'
import getSymbolForCode from '../../utils/currency'
require("!style!css!sass!./_meter.scss")

const createNode = type => {
	// creates an element with the specified namespace URI and qualified name
	return document.createElementNS("http://www.w3.org/2000/svg", type)
}

const setAttributes = (node, attributeList) => {
	// since we have a decent size of attributes then just loop
	// over for conveinence
	for (let [key, value] of attributeList) {
		node.setAttribute(key, value)
	}
}

const drawGradient = () => {
	// a gradient needs the linearGradient node and
	// two child stop nodes
	let gradient = createNode('linearGradient')
	let initialStop = createNode('stop')
	let endStop = createNode('stop')

	setAttributes(gradient, new Map([
		['id', 'linear'],
		['x1', '0%'],
		['y1', '0%'],
		['x2', '100%'],
		['y2', '0%']
	]))

	setAttributes(initialStop, new Map([
		['offset', '0%'],
		['stop-color', '#0a5']
	]))

	setAttributes(endStop, new Map([
		['offset', '100%'],
		['stop-color', '#05a']
	]))

	gradient.appendChild(initialStop)
	gradient.appendChild(endStop)

	return gradient
}

const drawText = ({value, coords = {}, offset = {}}) => {

	let textAttributes = new Map([
		['text-anchor', 'middle'],
		['x', (coords.x || 0) + (offset.x || 0)],
		['y', (coords.y || 0) + (offset.y || 0)]
	])

	let range = createNode('text')
	// set the textual content
	range.textContent = value
	setAttributes(range, textAttributes)

	return range
}

const drawMarker = ({coords, min, max, value, width}) => {

	let marker = createNode('path')
	let { x, y } = coords
	let offset = width ? (width / 2) + 85 : 0
	// determine angle of rotation
	let rotation = value < min || value > max || min > max ? 0 : (180 / (max - min)) * (value - min)

	let markerAttributes = new Map([
		['id', 'marker'],
		['d', `M${x + offset} ${y} c0.225 0 0.451-0.045 0.676-0.090 0.451-0.045 0.901-0.135 1.307-0.18 1.307-0.316 3.425-0.946 5.634-2.479 0.361-0.225 2.885-2.028 4.417-4.552 4.372-7.121 2.028-16.496-5.228-20.958-4.146-2.524-8.969-2.885-13.161-1.397-0.316 0.090-0.676 0.135-0.946 0.225l-67.651 21.318 1.668 8.248 73.284-0.135z`],
		['fill', '#e3e3e3'],
		['transform', `rotate(${rotation} ${x + offset} ${y - 15})`],
		['data-ignore', true],
		['stroke', '#cfcfcf']
	])

	setAttributes(marker, markerAttributes)

	return marker
}

const getFormattedValues = ({min, max, value, format, unit}) => {
	// when the format is currency then prepend the min, max, and value
	// with the correct currency symbol
	if (format === 'currency') {
		let symbol = getSymbolForCode(unit) || ''
		return {
			min: `${symbol}${min}`,
			max: `${symbol}${max}`,
			value: `${symbol}${value}`
		}
	}

	return { min, max, value }
}

export default class Meter {
	// setup some default values
	constructor({ rx = 150, ry = 150, xRotation = 0, lArc = 0, sweep = 1, x = 0, y = 0, range = {} }) {
		// position the arc in the center of the window
		this.dx = window.innerWidth/2 - rx || x
		this.dy = window.innerHeight/2 + ry/2 || y
		this.range = range

		// A rx ry x-axis-rotation large-arc-flag sweep-flag x y
		this.arc = new Map([
			['rx', rx],
			['ry', ry],
			['xRotation', xRotation],
			['lArc', lArc],
			['sweep', sweep],
			['x', rx + ry + this.dx],
			['y', this.dy]
		])
	}

	draw(node, options) {

		let svg = createNode('svg')
		let path = createNode('path')
		let { min, max, value } = this.range

		svg.setAttribute('id', 'meter')
		// override default animation with arguments if animation provided
		let animation = options.animation && Object.assign({}, {
			duration: 100,
			animTimingFunction: Vivus.EASE_IN
		}, options.animation)

		// set arc attributes for path
		let arcPathAttributes = this.getArcAttributes(this.arc)
		// set arc styles
		let arcAttributes = new Map([
			['d', `M${this.dx} ${this.dy} A${arcPathAttributes}`],
			['fill', options.fill || 'none'],
			['stroke', options.stroke || 'url(#linear)'],
			['stroke-width', options.strokeWidth || '12'],
			['stroke-opacity', options.strokeOpacity || '0.5']
		])

		setAttributes(path, arcAttributes)

		svg.append(drawGradient())

		// if all range values exist then draw them in the appropriate locations
		if (min && max && value) {

			let coords = {
				x: this.dx + this.arc.get('rx'),
				y: this.dy - this.arc.get('ry')
			}
			let formatted = getFormattedValues(this.range)
			svg.appendChild(drawText({value: formatted.value, coords, offset: {y: -12}}))
			svg.appendChild(drawText({value: formatted.min, coords: {x: this.dx, y: this.dy}, offset: {y: 20}}))
			svg.appendChild(drawText({value: formatted.max, coords: {x: this.arc.get('x'), y: this.arc.get('y')}, offset: {y: 20}}))

			// push in the marker
			// in the scenario that we receive bad data then dont rotate the marker
			svg.appendChild(drawMarker({
				coords: {
					x: this.dx,
					y: this.dy
				},
				value,
				min,
				max,
				width: this.arc.get('rx')
			}))
		}

		// push in the path
		svg.appendChild(path)

		if (options.error) {
			svg.appendChild(drawText({value: 'Something bad happened!', coords: {x: this.dx + this.arc.get('rx'), y: this.dy}}))
		}

		// append to the container
		node.appendChild(svg)
		// if animation was provided then animate the svg path
		options.animation && new Vivus('meter', animation)
	}

	getArcAttributes(iterator) {
		// since we have the arc attributes in a map we need to convert to a string
		// when building the arc itself
		return [...iterator].reduce((a, b) => {
			return `${Array.isArray(a) ? a[1] : a} ${b[1]}`
		})
	}
}

export {
	createNode,
	setAttributes,
	drawGradient,
	drawText,
	drawMarker,
	getFormattedValues
}
