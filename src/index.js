import fetchData from './utils/fetch'
import Meter from './components/meter/meter'

fetchData('meter', (response) => {
	let meter1 = new Meter({range: response})
	meter1.draw(document.getElementById('app'), {animation: true})
}, () => {
	let meter1 = new Meter({})
	meter1.draw(document.getElementById('app'), {stroke: '#e3e3e3', error: true})
})
