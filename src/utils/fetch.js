import 'whatwg-fetch'
import apis from 'apis'

const onSuccess = response => response

const onError = err => {
	console.error('error', err)
}

const checkStatus = response => {
	if (response.status >= 200 && response.status < 300 && !response.error) {
		return response
	}
	throw new Error(response.error || 'Something went wrong')
}

const getJSON = response => response.json()

// wrap fetch and override default callbacks
const fetchData = (endpoint, success = onSuccess, err = onError) => {
	// proxy all requests that begin with /api
	return fetch(apis[endpoint], {
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
		})
		.then(checkStatus)
		.then(getJSON)
		.then(success)
		.catch(err)
}

export default fetchData;
