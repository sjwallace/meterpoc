import currencyLookup from '../json/currencySymbols'

const getSymbolForCode = code => currencyLookup[code] || ''

export default getSymbolForCode
