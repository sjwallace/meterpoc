import { drawText } from '../components/meter/meter'

describe('drawText', () => {

	it('should create a text node when called', () => {
		let node = drawText({'value': 'foo', coords: {}})
		expect(node.tagName).toEqual('text')
		expect(node.textContent).toEqual('foo')
	})

	it('should create a text node at 0,0 when no coordinates are provided', () => {
		let node = drawText({'value': 'foo', coords: {}})
		expect(node.getAttribute('x')).toEqual('0')
		expect(node.getAttribute('y')).toEqual('0')
	})

	it('should create a text node at a given x,y coordinate when coordinates are provided', () => {
		let node = drawText({'value': 'foo', coords: {x: 1, y: 1}})
		expect(node.getAttribute('x')).toEqual('1')
		expect(node.getAttribute('y')).toEqual('1')
	})

	it('should create a text node at 0,0 when no coordinates are provided and no offset is given', () => {
		let node = drawText({'value': 'foo', coords: {}})
		expect(node.getAttribute('x')).toEqual('0')
		expect(node.getAttribute('y')).toEqual('0')
	})

	it('should create a text node at a given x,y when an offset is given', () => {
		let node = drawText({'value': 'foo', coords: {}, offset: {x: 1, y: 1}})
		expect(node.getAttribute('x')).toEqual('1')
		expect(node.getAttribute('y')).toEqual('1')
	})
})
