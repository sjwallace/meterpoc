import { setAttributes } from '../components/meter/meter'

describe('setAttributes', () => {

	let node = {};
	let setAttributeSpy = jasmine.createSpy('setAttributeSpy')
	let attributeMap = new Map([
		['foo', 'x'],
		['bar', 'y']
	])

	beforeAll(() => {
		node.setAttribute = (key, value) => {
			setAttributeSpy(key, value)
		}
	})

	afterEach(() => {
		setAttributeSpy.calls.reset()
	})

	it('should call a node\'s setAttribute method with the attribute key and value', () => {

		setAttributes(node, attributeMap)
		expect(setAttributeSpy.calls.count()).toEqual(2)
		expect(setAttributeSpy).toHaveBeenCalledWith('foo', 'x')
		expect(setAttributeSpy).toHaveBeenCalledWith('bar', 'y')
	})
})
