import { getFormattedValues } from '../components/meter/meter'

describe('getFormattedValues', () => {

	it('should return an object with min, max, and value properties', () => {

		expect(getFormattedValues({min: 0, max: 100, value: 50})).toEqual({
			min: 0, max: 100, value: 50
		})
	})

	it('should return an object with min, max, and value properties without formatting when the format is not specified', () => {

		expect(getFormattedValues({min: 0, max: 100, value: 50})).toEqual({
			min: 0, max: 100, value: 50
		})
	})

	it('should return an object with min, max, and value properties without formatting when the format is not equal to currency', () => {

		expect(getFormattedValues({min: 0, max: 100, value: 50, format: 'foo'})).toEqual({
			min: 0, max: 100, value: 50
		})
	})

	it('should return formatted values for min, max, and value when the format is of type currency and the unit can be found', () => {

		expect(getFormattedValues({min: 0, max: 100, value: 50, format: 'currency', unit: 'USD'})).toEqual({
			min: '$0', max: '$100', value: '$50'
		})
	})

	it('should return non formatted values for min, max, and value when the format is of type currency and the unit can not be found', () => {

		expect(getFormattedValues({min: 0, max: 100, value: 50, format: 'currency', unit: 'foo'})).toEqual({
			min: '0', max: '100', value: '50'
		})
	})
})
