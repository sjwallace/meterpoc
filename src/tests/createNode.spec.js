import { createNode } from '../components/meter/meter'

describe('createNode', () => {

	it('should create a document element with a specified namespace URI and node type', () => {
		expect(createNode('svg').tagName).toEqual('svg')
		expect(createNode('path').tagName).toEqual('path')
		expect(createNode('text').tagName).toEqual('text')
	})
})
