// require all `test/components`
const testsContext = require.context('.', true, /spec\.js$/);

testsContext.keys().forEach(testsContext);
// require all `src/components`
const componentsContext = require.context('../../src/components', true, /\.jsx?$/);

componentsContext.keys().forEach(componentsContext);
