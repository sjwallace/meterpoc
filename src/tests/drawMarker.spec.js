import { drawMarker } from '../components/meter/meter'

describe('drawMarker', () => {

	let props = {}

	beforeEach(() => {
		props = {
			coords: {
				x: 0,
				y: 0
			},
			value: 50,
			min: 0,
			max: 100
		}
	})

	it('should create a path element', () => {
		let node = drawMarker(props)
		expect(node.tagName).toEqual('path')
	})

	it('should rotate a path element based on the min, max, and value parameters', () => {
		let node = drawMarker(props)
		expect(node.getAttribute('transform')).toEqual('rotate(90 0 -15)')

		props.value = 180
		props.min = 0
		props.max = 180

		node = drawMarker(props)

		expect(node.getAttribute('transform')).toEqual('rotate(180 0 -15)')

		props.value = 45
		props.min = 0

		node = drawMarker(props)

		expect(node.getAttribute('transform')).toEqual('rotate(45 0 -15)')
	})

	it('should should not rotate a path element based on the values passed in', () => {
		let node = drawMarker(props)
		expect(node.getAttribute('transform')).toEqual('rotate(90 0 -15)')

		// value > max
		props.value = 200;
		node = drawMarker(props)
		expect(node.getAttribute('transform')).toEqual('rotate(0 0 -15)')

		// value < min
		props.min = 100;
		props.value = 10;
		node = drawMarker(props)
		expect(node.getAttribute('transform')).toEqual('rotate(0 0 -15)')

		// min > max
		props.max = 10;
		node = drawMarker(props)
		expect(node.getAttribute('transform')).toEqual('rotate(0 0 -15)')
	})
})
